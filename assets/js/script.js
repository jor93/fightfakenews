/*
****************************************************************************************************
****** 	Author: Robert Johner																  ******
****** 	Date: 15.08.2020																	  ******
****** 	Version: 1.0																		  ******
****** 	File: script.js																		  ******
****************************************************************************************************

Change History:
1.0 Initial version

*/

/***************************************************************************
********************************   INIT  ***********************************
*****************************************************************************/

// regex with bad words that are prohibited from using as a player name and will be replaced
const rBadWords = /\b(abbo|abo|abortion|abuse|addict|addicts|adult|africa|african|alla|allah|alligatorbait|amateur|american|anal|analannie|analsex|angie|angry|anus|arab|arabs|areola|argie|aroused|arse|arsehole|asian|ass|assassin|assassinate|assassination|assault|assbagger|assblaster|assclown|asscowboy|asses|assfuck|assfucker|asshat|asshole|assholes|asshore|assjockey|asskiss|asskisser|assklown|asslick|asslicker|asslover|assman|assmonkey|assmunch|assmuncher|asspacker|asspirate|asspuppies|assranger|asswhore|asswipe|athletesfoot|attack|australian|babe|babies|backdoor|backdoorman|backseat|badfuck|balllicker|balls|ballsack|banging|baptist|barelylegal|barf|barface|barfface|bast|bastard|bazongas|bazooms|beaner|beast|beastality|beastial|beastiality|beatoff|beat-off|beatyourmeat|beaver|bestial|bestiality|bi|biatch|bible|bicurious|bigass|bigbastard|bigbutt|bigger|bisexual|bi-sexual|bitch|bitcher|bitches|bitchez|bitchin|bitching|bitchslap|bitchy|biteme|black|blackman|blackout|blacks|blind|blow|blowjob|boang|bogan|bohunk|bollick|bollock|bomb|bombers|bombing|bombs|bomd|bondage|boner|bong|boob|boobies|boobs|booby|boody|boom|boong|boonga|boonie|booty|bootycall|bountybar|bra|brea5t|breast|breastjob|breastlover|breastman|brothel|bugger|buggered|buggery|bullcrap|bulldike|bulldyke|bullshit|bumblefuck|bumfuck|bunga|bunghole|buried|burn|butchbabes|butchdike|butchdyke|butt|buttbang|butt-bang|buttface|buttfuck|butt-fuck|buttfucker|butt-fucker|buttfuckers|butt-fuckers|butthead|buttman|buttmunch|buttmuncher|buttpirate|buttplug|buttstain|byatch|cacker|cameljockey|cameltoe|canadian|cancer|carpetmuncher|carruth|catholic|catholics|cemetery|chav|cherrypopper|chickslick|children's|chin|chinaman|chinamen|chinese|chink|chinky|choad|chode|christ|christian|church|cigarette|cigs|clamdigger|clamdiver|clit|clitoris|clogwog|cocaine|cock|cockblock|cockblocker|cockcowboy|cockfight|cockhead|cockknob|cocklicker|cocklover|cocknob|cockqueen|cockrider|cocksman|cocksmith|cocksmoker|cocksucer|cocksuck|cocksucked|cocksucker|cocksucking|cocktail|cocktease|cocky|cohee|coitus|color|colored|coloured|commie|communist|condom|conservative|conspiracy|coolie|cooly|coon|coondog|copulate|cornhole|corruption|cra5h|crabs|crack|crackpipe|crackwhore|crack-whore|crap|crapola|crapper|crappy|crash|creamy|crime|crimes|criminal|criminals|crotch|crotchjockey|crotchmonkey|crotchrot|cum|cumbubble|cumfest|cumjockey|cumm|cummer|cumming|cumquat|cumqueen|cumshot|cunilingus|cunillingus|cunn|cunnilingus|cunntt|cunt|cunteyed|cuntfuck|cuntfucker|cuntlick|cuntlicker|cuntlicking|cuntsucker|cybersex|cyberslimer|dago|dahmer|dammit|damn|damnation|damnit|darkie|darky|datnigga|dead|deapthroat|death|deepthroat|defecate|dego|demon|deposit|desire|destroy|deth|devil|devilworshipper|dick|dickbrain|dickforbrains|dickhead|dickless|dicklick|dicklicker|dickman|dickwad|dickweed|diddle|die|died|dies|dike|dildo|dingleberry|dink|dipshit|dipstick|dirty|disease|diseases|disturbed|dive|dix|dixiedike|dixiedyke|doggiestyle|doggystyle|dong|doodoo|doo-doo|doom|dope|dragqueen|dragqween|dripdick|drug|drunk|drunken|dumb|dumbass|dumbbitch|dumbfuck|dyefly|dyke|easyslut|eatballs|eatme|eatpussy|ecstacy|ejaculate|ejaculated|ejaculating|ejaculation|enema|enemy|erect|erection|ero|escort|ethiopian|ethnic|european|evl|excrement|execute|executed|execution|executioner|explosion|facefucker|faeces|fag|fagging|faggot|fagot|failed|failure|fairies|fairy|faith|fannyfucker|fart|farted|farting|farty|fastfuck|fat|fatah|fatass|fatfuck|fatfucker|fatso|fckcum|fear|feces|felatio|felch|felcher|felching|fellatio|feltch|feltcher|feltching|fetish|fight|filipina|filipino|fingerfood|fingerfuck|fingerfucked|fingerfucker|fingerfuckers|fingerfucking|fire|firing|fister|fistfuck|fistfucked|fistfucker|fistfucking|fisting|flange|flasher|flatulence|floo|flydie|flydye|fok|fondle|footaction|footfuck|footfucker|footlicker|footstar|fore|foreskin|forni|fornicate|foursome|fourtwenty|fraud|freakfuck|freakyfucker|freefuck",,"fucck|fuck|fucka|fuckable|fuckbag|fuckbuddy|fucked|fuckedup|fucker|fuckers|fuckface|fuckfest|fuckfreak|fuckfriend|fuckhead|fuckher|fuckin|fuckina|fucking|fuckingbitch|fuckinnuts|fuckinright|fuckit|fuckknob|fuckme|fuckmehard|fuckmonkey|fuckoff|fuckpig|fucks|fucktard|fuckwhore|fuckyou|fudgepacker|fugly|fuk|fuks|funeral|funfuck|fungus|fuuck|gangbang|gangbanged|gangbanger|gangsta|gatorbait|gay|gaymuthafuckinwhore|gaysex|geez|geezer|geni|genital|german|getiton|gin|ginzo|gipp|girls|givehead|glazeddonut|gob|god|godammit|goddamit|goddammit|goddamn|goddamned|goddamnes|goddamnit|goddamnmuthafucker|goldenshower|gonorrehea|gonzagas|gook|gotohell|goy|goyim|greaseball|gringo|groe|gross|grostulation|gubba|gummer|gun|gyp|gypo|gypp|gyppie|gyppo|gyppy|hamas|handjob|hapa|harder|hardon|harem|headfuck|headlights|hebe|heeb|hell|henhouse|heroin|herpes|heterosexual|hijack|hijacker|hijacking|hillbillies|hindoo|hiscock|hitler|hitlerism|hitlerist|hiv|ho|hobo|hodgie|hoes|hole|holestuffer|homicide|homo|homobangers|homosexual|honger|honk|honkers|honkey|honky|hook|hooker|hookers|hooters|hore|hork|horn|horney|horniest|horny|horseshit|hosejob|hoser|hostage|hotdamn|hotpussy|hottotrot|hummer|husky|hussy|hustler|hymen|hymie|iblowu|idiot|ikey|illegal|incest|insest|intercourse|interracial|intheass|inthebuff|israel|israeli|israel's|italiano|itch|jackass|jackoff|jackshit|jacktheripper|jade|jap|japanese|japcrap|jebus|jeez|jerkoff|jesus|jesuschrist|jew|jewish|jiga|jigaboo|jigg|jigga|jiggabo|jigger|jiggy|jihad|jijjiboo|jimfish|jism|jiz|jizim|jizjuice|jizm|jizz|jizzim|jizzum|joint|juggalo|jugs|junglebunny|kaffer|kaffir|kaffre|kafir|kanake|kid|kigger|kike|kill|killed|killer|killing|kills|kink|kinky|kissass|kkk|knife|knockers|kock|kondum|koon|kotex|krap|krappy|kraut|kum|kumbubble|kumbullbe|kummer|kumming|kumquat|kums|kunilingus|kunnilingus|kunt|ky|kyke|lactate|laid|lapdance|latin|lesbain|lesbayn|lesbian|lesbin|lesbo|lez|lezbe|lezbefriends|lezbo|lezz|lezzo|liberal|libido|licker|lickme|lies|limey|limpdick|limy|lingerie|liquor|livesex|loadedgun|lolita|looser|loser|lotion|lovebone|lovegoo|lovegun|lovejuice|lovemuscle|lovepistol|loverocket|lowlife|lsd|lubejob|lucifer|luckycammeltoe|lugan|lynch|macaca|mad|mafia|magicwand|mams|manhater|manpaste|marijuana|mastabate|mastabater|masterbate|masterblaster|mastrabator|masturbate|masturbating|mattressprincess|meatbeatter|meatrack|meth|mexican|mgger|mggor|mickeyfinn|mideast|milf|minority|mockey|mockie|mocky|mofo|moky|moles|molest|molestation|molester|molestor|moneyshot|mooncricket|mormon|moron|moslem|mosshead|mothafuck|mothafucka|mothafuckaz|mothafucked|mothafucker|mothafuckin|mothafucking|mothafuckings|motherfuck|motherfucked|motherfucker|motherfuckin|motherfucking|motherfuckings|motherlovebone|muff|muffdive|muffdiver|muffindiver|mufflikcer|mulatto|muncher|munt|murder|murderer|muslim|naked|narcotic|nasty|nastybitch|nastyho|nastyslut|nastywhore|nazi|necro|negro|negroes|negroid|negro's|nig|niger|nigerian|nigerians|nigg|nigga|niggah|niggaracci|niggard|niggarded|niggarding|niggardliness|niggardliness's|niggardly|niggards|niggard's|niggaz|nigger|niggerhead|niggerhole|niggers|nigger's|niggle|niggled|niggles|niggling|nigglings|niggor|niggur|niglet|nignog|nigr|nigra|nigre|nip|nipple|nipplering|nittit|nlgger|nlggor|nofuckingway|nook|nookey|nookie|noonan|nooner|nude|nudger|nuke|nutfucker|nymph|ontherag|oral|orga|orgasim|orgasm|orgies|orgy|osama|paki|palesimian|palestinian|pansies|pansy|panti|panties|payo|pearlnecklace|peck|pecker|peckerwood|pee|peehole|pee-pee|peepshow|peepshpw|pendy|penetration|peni5|penile|penis|penises|penthouse|period|perv|phonesex|phuk|phuked|phuking|phukked|phukking|phungky|phuq|pi55|picaninny|piccaninny|pickaninny|piker|pikey|piky|pimp|pimped|pimper|pimpjuic|pimpjuice|pimpsimp|pindick|piss|pissed|pisser|pisses|pisshead|pissin|pissing|pissoff|pistol|pixie|pixy|playboy|playgirl|pocha|pocho|pocketpool|pohm|polack|pom|pommie|pommy|poo|poon|poontang|poop|pooper|pooperscooper|pooping|poorwhitetrash|popimp|porchmonkey|porn|pornflick|pornking|porno|pornography|pornprincess|pot|poverty|premature|pric|prick|prickhead|primetime|propaganda|pros|prostitute|protestant|pu55i|pu55y|pube|pubic|pubiclice|pud|pudboy|pudd|puddboy|puke|puntang|purinapricness|puss|pussie|pussies|pussy|pussycat|pussyeater|pussyfucker|pussylicker|pussylips|pussylover|pussypounder|pusy|quashie|queef|queer|quickie|quim|ra8s|rabbi|racial|racist|radical|radicals|raghead|randy|rape|raped|raper|rapist|rearend|rearentry|rectum|redlight|redneck|reefer|reestie|refugee|reject|remains|rentafuck|republican|rere|retard|retarded|ribbed|rigger|rimjob|rimming|roach|robber|roundeye|rump|russki|russkie|sadis|sadom|samckdaddy|sandm|sandnigger|satan|scag|scallywag|scat|schlong|screw|screwyou|scrotum|scum|semen|seppo|servant|sex|sexed|sexfarm|sexhound|sexhouse|sexing|sexkitten|sexpot|sexslave|sextogo|sextoy|sextoys|sexual|sexually|sexwhore|sexy|sexymoma|sexy-slim|shag|shaggin|shagging|shat|shav|shawtypimp|sheeney|shhit|shinola|shit|shitcan|shitdick|shite|shiteater|shited|shitface|shitfaced|shitfit|shitforbrains|shitfuck|shitfucker|shitfull|shithapens|shithappens|shithead|shithouse|shiting|shitlist|shitola|shitoutofluck|shits|shitstain|shitted|shitter|shitting|shitty|shoot|shooting|shortfuck|showtime|sick|sissy|sixsixsix|sixtynine|sixtyniner|skank|skankbitch|skankfuck|skankwhore|skanky|skankybitch|skankywhore|skinflute|skum|skumbag|slant|slanteye|slapper|slaughter|slav|slave|slavedriver|sleezebag|sleezeball|slideitin|slime|slimeball|slimebucket|slopehead|slopey|slopy|slut|sluts|slutt|slutting|slutty|slutwear|slutwhore|smack|smackthemonkey|smut|snatch|snatchpatch|snigger|sniggered|sniggering|sniggers|snigger's|sniper|snot|snowback|snownigger|sob|sodom|sodomise|sodomite|sodomize|sodomy|sonofabitch|sonofbitch|sooty|sos|soviet|spaghettibender|spaghettinigger|spank|spankthemonkey|sperm|spermacide|spermbag|spermhearder|spermherder|spic|spick|spig|spigotty|spik|spit|spitter|splittail|spooge|spreadeagle|spunk|spunky|squaw|stagg|stiffy|strapon|stringer|stripclub|stroke|stroking|stupid|stupidfuck|stupidfucker|suck|suckdick|sucker|suckme|suckmyass|suckmydick|suckmytit|suckoff|suicide|swallow|swallower|swalow|swastika|sweetness|syphilis|taboo|taff|tampon|tang|tantra|tarbaby|tard|teat|terror|terrorist|teste|testicle|testicles|thicklips|thirdeye|thirdleg|threesome|threeway|timbernigger|tinkle|tit|titbitnipply|titfuck|titfucker|titfuckin|titjob|titlicker|titlover|tits|tittie|titties|titty|tnt|toilet|tongethruster|tongue|tonguethrust|tonguetramp|tortur|torture|tosser|towelhead|trailertrash|tramp|trannie|tranny|transexual|transsexual|transvestite|triplex|trisexual|trojan|trots|tuckahoe|tunneloflove|turd|turnon|twat|twink|twinkie|twobitwhore|uck|uk|unfuckable|upskirt|uptheass|upthebutt|urinary|urinate|urine|usama|uterus|vagina|vaginal|vatican|vibr|vibrater|vibrator|vietcong|violence|virgin|virginbreaker|vomit|vulva|wab|wank|wanker|wanking|waysted|weapon|weenie|weewee|welcher|welfare|wetb|wetback|wetspot|whacker|whash|whigger|whiskey|whiskeydick|whiskydick|whit|whitenigger|whites|whitetrash|whitey|whiz|whop|whore|whorefucker|whorehouse|wigger|willie|williewanker|willy|wn|wog|women's|wop|wtf|wuss|wuzzie|xtc|xxx|yankee|yellowman|zigabo|zipperhead|analritter|arsch|arschficker|arschlecker|arschloch|bimbo|bratze|bumsen|bonze|dödel|fick|ficken|flittchen|fotze|fratze|hackfresse|hure|hurensohn|ische|kackbratze|kacke|kacken|kackwurst|kampflesbe|kanake|kimme|lümmel|MILF|möpse|morgenlatte|möse|mufti|muschi|nackt|neger|nigger|nippel|nutte|onanieren|orgasmus|penis|pimmel|pimpern|pinkeln|pissen|pisser|popel|poppen|porno|reudig|rosette|schabracke|schlampe|scheiße|scheisser|schiesser|schnackeln|schwanzlutscher|schwuchtel|tittchen|titten|vögeln|vollpfosten|wichse|wichsen|wichser)\b/g;

// player model
var oPlayerModel = {
  // current avatar index
  iAvatarIndex: 0,
  // first avatar image file
  sAvatarImage: "assets/img/avatars/0.png",
  // player name
  sPlayerName: "",
  // check game status
  bIsPlaying: true,
  // check sound status
  bIsMuted: true,
  // points earned
  iPoints: 0,
  // game mode
  sGameMode: "",
  // game mode selected
  sGameModeSelected: "",
};

// sound model
var oSoundEffectModel = {
  // correct answer sound
  oCorrect: new Audio("assets/sound/correct.mp3"),
  // wrong answer sound
  oWrong: new Audio("assets/sound/wrong.mp3"),
  // netural sound when time ran out
  oNeutral: new Audio("assets/sound/neutral.mp3"),
  // sound effect when action was triggered
  oButton: new Audio("assets/sound/button.mp3"),
  // sound effect when switching questions
  oSwipe: new Audio("assets/sound/swipe.mp3"),
  // sound effect when timer is running low
  oCountDown: new Audio("assets/sound/countdown.mp3")
};

// background sound
var oSound = document.getElementById("sound"),
// trigger for countdown
bCountDownPlaying = false;

// player model
var oGameModel = {
  // current screen index
  iCurrentScreen: 0,
  // current detective question
  iCurrentDetective: 0,
  // current creator question
  iCurrentCreator: 0,
  // current selected language
  sCurrentLang: "",
  // counter for badge creator
  iTotalCreator: 0,
  // counter for badge detective
  iTotalDetective: 0,
  // counter for badge games played
  iTotalGames: 0,
  // counter for badge points
  iTotalPoints: 0,
  // counter for badge learner
  iTotalLearner: 0,
};



// game data loaded from json
var oGameData,
// timer reference
oTimer,
// all screns
screens = $(".screen"),
iPointsPerQuestion,
iPointsPerArticle,
iTimePerQuestion,
iCreatorPerGame,
iDetectivePerGame;

// game questions
var oGameQuestions = [];

// prepare game and show first screen to user
prepareGame();


/***************************************************************************
*****************************   NAVIGATION  ********************************
*****************************************************************************/

/**
* Prepares game data and sets up the view for the first time.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function prepareGame() {
  // try to autoplay background music
  _autoplayAudio();
  // show screen init game (0)
  showScreen(0);

  // load game data from json
  oGameData = $.ajax({
    url: 'assets/data/data.json',
    async: false,
    dataType: 'json'
  }).responseJSON;

  // check current language --> default value is english
  var selectedLanguage = $("#choose-language").val();
  // save selected language in game oGameModel
  oGameModel.sCurrentLang = selectedLanguage;

  // attach key listener to start game button. Game will start when enter is pressed
  $("#username").keyup(function (e) {
    if (e.keyCode == 13) {
      // start game
      onClickedInitGame();
    }
  });

  // set feature is comming soon to button
  $(".invite-icon, .share-highscores-button").click(function () {
    $(this).addClass('clicked');
    setTimeout(() => {
      $(this).removeClass('clicked');
    }, 3000);
  });

  // set avatar from player model. In the first run the default avatar will be displayed. If the user has changed the avatar, the new avatar will be displayed.
  $("#avatar").attr("src", oPlayerModel.sAvatarImage);

  // to store random questions for current user
  aDetective = [],
  // to store random questions for current user
  aCreator = [],

  iTimerWidth = 100,
  // load default values
  iPointsPerQuestion = oGameData.data.settings[0].points,
  iPointsPerArticle =  oGameData.data.settings[0].pointsCreator;
  iTimePerQuestion = oGameData.data.settings[0].time,
  iCreatorPerGame = oGameData.data.settings[0].detective,
  iDetectivePerGame = oGameData.data.settings[0].creator;
  // to store badges
  aBadges = [];
}

/**
* General navigation to switch between different screens and enable basic navigation.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
* @param {iINdex} [Integer] the index of the screen you want to show
*/
function showScreen(iIndex) {
  // set new screen in game oGameModel
  oGameModel.iCurrentScreen = iIndex;
  // iterate over all possible screens to update view correctly
  screens.each(function (index) {
    // check index to show correct screen
    if (index !== iIndex) {
      // hide view
      $(this).hide();
    } else {
      // show view
      $(this).show();
    }
  });

  // do screen specific UI handling
  switch (iIndex) {
    // init game
    case 0:
    // set background image
    _setBackground("body-bg-red");
    // show language changer
    _showLanguageChanger(true);
    // hide current user
    _showCurrentUser(false);
    // hide close button
    _showCloseButton(false);
    // hide go back button
    _showGoBackButton(false);
    // hide invite button
    _showInviteButton(false);
    // hide points label
    _showPointsLabel(true);
    break;

    // start game
    case 1:
    // set background image
    _setBackground("body-bg-avatar");
    // show language changer
    _showLanguageChanger(true);
    // show current user
    _showCurrentUser(true);
    // show close button
    _showCloseButton(true);
    // hide go back button
    _showGoBackButton(false);
    // show invite buttons
    _showInviteButton(true);
    // show points label
    _showPointsLabel(true);
    break;

    // FAQ
    case 2:
    // set background image
    _setBackground("body-bg-white");
    // show language changer
    _showLanguageChanger(true);
    // show current user
    _showCurrentUser(true);
    // show close button
    _showCloseButton(true);
    // hide go back button
    _showGoBackButton(true);
    // show invite buttons
    _showInviteButton(false);
    // show points label
    _showPointsLabel(true);
    // load entries from data.json
    _loadHelp();
    break;

    // choose game mode
    case 3:
    // set background image
    _setBackground("body-bg-white");
    // show language changer
    _showLanguageChanger(true);
    // show current user
    _showCurrentUser(true);
    // show close button
    _showCloseButton(true);
    // hide go back button
    _showGoBackButton(true);
    // show invite buttons
    _showInviteButton(false);
    // show points label
    _showPointsLabel(true);
    // link hover to game mode buttons
    $('.game-mode-box').hover(
      function () {
        $('.game-mode-box').addClass('hovering');
        $(this).addClass('hovering-this');
      },
      function () {
        $('.game-mode-box').removeClass('hovering');
        $(this).removeClass('hovering-this');
      }
    )
    break;

    // detective
    case 4:
    // set background image
    _setBackground("body-bg-white");
    // hide language changer
    _showLanguageChanger(false);
    // hide go back button
    _showGoBackButton(false);
    // show points label
    _showPointsLabel(true);
    // start detective mode
    _playAsDetective();
    break;

    // creator
    case 5:
    // set background image
    _setBackground("body-bg-white");
    // hide language changer
    _showLanguageChanger(false);
    // hide go back button
    _showGoBackButton(false);
    // show points label
    _showPointsLabel(true);
    // start creator mode
    _playAsCreator();
    break;

    // result page
    case 6:
    // hide language changer
    _showLanguageChanger(false);
    // show results
    _previewResults();
    break;
    default:
    break;

  }
}


/***************************************************************************
***************************   EVENT HANDLING  ******************************
*****************************************************************************/

/**
* Executes when the user presses the button to go back. Plays the button clicked sound if the game is not muted and goes back 1 page according to the navigation. This button is only visible on the help page and choose game mode page.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedGoBack() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // switch between current screen to go back to the correct screen
  switch (oGameModel.iCurrentScreen) {
    case 1:
    showScreen(0);
    break;
    case 2:
    showScreen(1);
    break;
    case 3:
    showScreen(1);
    break;
  }
}

/**
* Executes when the user presses the button to close the game. Plays the button clicekd sound if the game is not muted.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedCloseGame() {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
}

/**
* Executes when the user presses the button yes on the close modal. Plays the button clicekd sound if the game is not muted and restarts the game.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedModalYes() {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
  // reload page to restart game
  location.reload();
}

/**
* Executes when the user presses the button no or close on the close modal. Plays the button clicekd sound if the game is not muted.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedModalNoOrClose() {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
}

/**
* Executes when the user closes the badge modal
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedModalDismissBadge() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // get value from button
  var iScreen = parseInt($("#close-badge").val());
  // dismiss modal
  $("#earnedBadgeModal").modal("toggle");
  // check if user is already on correct screen
  if (iScreen !== oGameModel.iCurrentScreen) {
    // go screen 6 to show results
    showScreen(iScreen);
  }
}

/**
* Executes when the user closes the feedback modal
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedModalDismissFeedback() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // reset timer classes
  $(".progress-container").removeClass("green");
  $(".progress-container").removeClass("yellow");
  $(".progress-container").removeClass("red");
  // dismiss modal
  $("#feedbackModal").modal("toggle");
  // increase current creator
  oGameModel.iCurrentCreator++;
  // show next question
  _showNextCreatorQuestion();
}

/**
* Executes when the user changes the language. Applies the new langauge to the UI.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onChangedLanguage() {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
  // get new language
  var selectedLanguage = $("#choose-language").val();
  // save selected language in oGameModel
  oGameModel.sCurrentLang = selectedLanguage;
  // switch languages
  switch (oGameModel.sCurrentLang) {
    case 'en':
    // set i18n to english
    $.i18n({
      locale: 'en' // Locale is English
    });
    // apply english
    $('body').i18n();
    break;
    case 'de':
    // set i18n to de
    $.i18n({
      locale: 'de' // Locale is German
    });
    // apple german
    $('body').i18n();
    break;
  }
  // check if user is on help page to correctly change language
  if (oGameModel.iCurrentScreen === 2) {
    // reload page to change languge correctly
    showScreen(2);
  }
}

/**
* Executes when the user presses the button "mute/unmute" on all screens. Toggles background sound and sound effect on or off. Also changes the icon of the mute/unmutte button to display the correct state.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
* @param {oElement} [Element] the <i> element to change the icon
*/
function onClickedMute(oElement) {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
  // set new mute state
  oPlayerModel.bIsMuted = (oPlayerModel.bIsMuted ? false : true);

  // check current mute status
  if (oPlayerModel.bIsMuted) {
    // game is muted
    // pause background audio
    _pauseSound();
    // replace icon
    $(oElement).removeClass();
    $(oElement).addClass("fa fa-volume-mute red");
    // set mute button to <i> element
  } else {
    // game is not muted
    // play background audio
    _playSoundInLoop();
    // replace icon
    $(oElement).removeClass();
    $(oElement).addClass("fa fa-volume-up red");
  }
}

/**
* Executes when the user presses the button with the icon "fa-chevron-left" or "fa-chevron-right" on the screen "init game (0)". Checks which action the user has chosen and cycles either left or right through the different avatars avaiable. If the final
* avater is reached, the first is diplayed again and vice versa. The string value 'prev' indicates left and the string value 'next' indicates right.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
* @param {sAction} [String] the string action code
*/
function onClickedChangeAvatar(sAction) {
  // play button clicked sound effect
  _playSoundEffect(oSoundEffectModel.oButton);
  // get avatars
  var aAvatars = oGameData.data.avatars;
  // get current avatar
  var iIndex = oPlayerModel.iAvatarIndex;

  // check action code to calculate new index
  if (sAction === 'prev' ? iIndex = _prevAvatar(iIndex, aAvatars.length) : iIndex = _nextAvatar(iIndex, aAvatars.length));

  // set new index and image in player model
  oPlayerModel.iAvatarIndex = iIndex;
  oPlayerModel.sAvatarImage = aAvatars[iIndex].src;
  // update avatar on view
  $("#avatar").attr("src", aAvatars[iIndex].src);
}

/**
* Executes when the user presses the button "start game" on the screen "init game (0)". Checks if the user entered a player name and redirects to the user to the next screen.
* If no player name has been entered, a error message is shown to the user.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedInitGame() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // get textfield and button
  var oUsername = $("#username"),
  oButton = $("#start-game-button"),
  sUsername = oUsername.val().toLowerCase();

  // check if username value is not empty
  if (sUsername !== "" && sUsername.length !== 0 && sUsername.length < 21) {
    // look for all bad words
    var aMatches = sUsername.match(rBadWords);
    // check if any bad words have been found
    if (aMatches) {
      // replace each bad word with ***
      for (var i = 0; i < aMatches.length; i++) {
        // count how many chars the bad word has replace it by the corresponding amount of ***.
        sUsername = sUsername.replace(aMatches[i], new Array(aMatches[i].length + 1).join("*"));
      }
    }
    // hide username
    oUsername.next().remove();
    // save player name in player model
    oPlayerModel.sPlayerName = sUsername;
    // go to screen start game (1)
    showScreen(1);
  } else {
    // form validation error
    // shake oButton
    _shakeButton(oButton);
    // add username error message
    oUsername.after('<p style="display: none" id="error-username"></p>');
    // get error message object
    var oErrorUser = $("#error-username");
    // check if error message is not already displayed --> prevent from adding mutliple error messages
    if (oErrorUser.length == 0 || oErrorUser == undefined || oErrorUser.css("display") === "none") {
      // check which error to display
      if (sUsername.length === 0) oErrorUser.attr("data-i18n", "0_init_usernameErrorEmpty");
      else if (sUsername.length > 20) oErrorUser.attr("data-i18n", "0_init_usernameErrorTooLong");
      oErrorUser.i18n();
      // show animation on error object
      _showErrorMessageWithAnimation(oErrorUser);
    }
  }
}

/**
* Executes when the user presses the button "start game" on the screen "start game (1)". Navigates the user to the select game mode screen.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedStartGame() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // show screen
  showScreen(3);
}


/**
* Executes when the user presses the button "Help" on the screen "start game (1)". Navigates the user to the FAQ pages.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedFAQ() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // show screen 2
  showScreen(2);
}

/**
* Executes when the user presses the button to start a game mode
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedStartGameMode() {
  // check if a game mode was selected
  if (!oPlayerModel.sGameModeSelected) {
    return;
  }
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // set current game mode
  oPlayerModel.sGameMode = oPlayerModel.sGameModeSelected;
  // check which game mode was selected
  if (oPlayerModel.sGameMode === 'detective') {
    // go to detective mode
    showScreen(4);
  } else {
    // go to creator mode
    showScreen(5);
  }
}

/**
* Executes when the user presses the box "Detective" on the screen "Select Mode (3)" to start game mode.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedDetective() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // set mode in player model
  oPlayerModel.sGameModeSelected = $("#detective-game").attr("mode");
  // select detective box
  $('.game-mode-box').removeClass('selected');
  $("#detective-game").addClass('selected');
  // enable start game mode button
  $('.select-game-mode-button').removeClass('gray');
}

/**
* Executes when the user presses on an answer while playing as an detective.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
* @param {oElement} [Element] the <div> element
*/
function onClickedChoiceDetective(oElement) {
  // check if countdown is playing
  if (bCountDownPlaying) {
    // reset countdown sound
    oSoundEffectModel.oCountDown.pause();
    oSoundEffectModel.oCountDown.currentTime = 0;
    // reset trigger
    bCountDownPlaying = false;
  }
  // disable all choice buttons to prevent double answering
  $("#choices").find("button").each(function () { $(this).prop('disabled', true) });
  // stop timer
  _stopTimer();
  // get correct answer
  var sCorrectAnswer = $(oElement).attr('correct'),
  // get selected answer
  sSelectedAnswer = $(oElement).text(),
  // get question
  sQuestion = $(oElement).attr('question'),
  bCorrect = false;

  // check if selected answer was correct
  if (sCorrectAnswer === sSelectedAnswer) {
    // play correct sound
    _playSoundEffect(oSoundEffectModel.oCorrect);
    // set correct to true
    bCorrect = true;
    // update points in player model
    oPlayerModel.iPoints += iPointsPerQuestion;
    // add correct class to update on UI
    $(oElement).addClass("correct");
  } else {
    // answer not corect
    // play wrong sound
    _playSoundEffect(oSoundEffectModel.oWrong);
    // set correct to false
    bCorrect = false;
    // get all answers
    const aAnswers = document.getElementsByClassName("choice");
    // mark wrong answer red and correct answer green to give feedback to user
    for (let i = 0; i < aAnswers.length; i++) {
      if (aAnswers[i].innerText !== sCorrectAnswer) {
        aAnswers[i].classList.add("wrong");
      } else {
        aAnswers[i].classList.add("correct");
      }
    }
  }
  // save answer for result page
  aDetective[oGameModel.iCurrentDetective].isCorrect = bCorrect;

  // update progress on UI
  if ($(".current-question")[oGameModel.iCurrentDetective - 1]) {
    $(".current-question")[oGameModel.iCurrentDetective - 1].classList.remove("active");
  }

  if ($(".current-question")[oGameModel.iCurrentDetective]) {
    $(".current-question")[oGameModel.iCurrentDetective].classList.add("active");
  }
  // move to next question but show
  setTimeout(function () {
    // reset timer classes
    $(".progress-container").removeClass("green");
    $(".progress-container").removeClass("yellow");
    $(".progress-container").removeClass("red");
    // increase current detective index
    oGameModel.iCurrentDetective++;
    // play swipe sound
    _playSoundEffect(oSoundEffectModel.oSwipe);
    // update points
    _updatePoints();
    // show next detective question
    _showNextDetectiveQuestion();
  }, 1000);
}

/**
* Executes when the user presses the box "Creator" on the screen "Select Mode (3)". Starts the game mode creator.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedCreator() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // set mode in player model
  oPlayerModel.sGameModeSelected = $("#creator-game").attr("mode");
  // remove gray on click
  $('.game-mode-box').removeClass('selected');
  $("#creator-game").addClass('selected');
  $('.select-game-mode-button').removeClass('gray');
}

/**
* Executes when the user presses on an image in the creater mode.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedCreatorImg(oElement) {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // remove selected
  $('#images').addClass('selectedImage');
  $(".img-section-img").removeClass("selected");
  // set selected image as selected
  $(oElement).addClass("selected");
  // change visibility
  $(".article-section").css("visibility", "visible");
}

/**
* Executes when the user changes an article option
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onChangedArticleText() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // check if all text positions are filled out
  var bAllSelected = $(".slct").filter(function () {
    // return
    return !$(this).val();
  });
  // if all selected --> make next box visible
  if (bAllSelected.length === 0) {
    // make platforms visible
    $(".platform-section").css("visibility", "visible");
  }
}

/**
* Executes when the user presses on a platform.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedPlatform(oElement) {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // remove selected class
  $(".platform-icon").removeClass("selected");
  // make selected platform selected
  $(oElement).addClass("selected");
  // show publish button
  $(".publish-container").css("visibility", "visible");
}

/**
* Executes when the user presses the button to publish a fake news article.
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedPublish() {
  // check if countdown is playing
  if (bCountDownPlaying) {
    // reset countdown sound
    oSoundEffectModel.oCountDown.pause();
    oSoundEffectModel.oCountDown.currentTime = 0;
    // reset trigger
    bCountDownPlaying = false;
  }
  $(".publish-container").css("visibility", "hidden");
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // stop timer
  _stopTimer();
  // round to nearest 5
  const roundToNearest5 = x => Math.round(x/5)*5
  // get multiplier value from selected image
  var iImagePoints = aCreator[oGameModel.iCurrentCreator].image1.src === $(".img-section-img.selected img").attr('src') ? Number(aCreator[oGameModel.iCurrentCreator].image1.points) : Number(aCreator[oGameModel.iCurrentCreator].image2.points),
  // for image feedback
  bGoodImage = iImagePoints > 0;

  // get selected values
  var aSelect = document.getElementsByClassName("slct"),
  // article multiplier
  iArticlePoints = 0,
  // for article feedback
  bGoodArticles = true;

  // get multipliers from all choices
  for (let i = 0; i < aSelect.length; i++) {
    // check if bad selection has been made
    if(aSelect[i].value < 0) bGoodArticles = false;
    iArticlePoints += Number(aSelect[i].value);
  }
  // get selected platform
  var sPlatform = $(".platform-icon.selected").attr("value"),
  // get multiplier for selected platform
  iPlatformPoints = aCreator[oGameModel.iCurrentCreator].platformsPoints.filter(el => {
    return el[sPlatform];
  })[0][sPlatform],
  // for platform feedback
  bGoodPlatform = iPlatformPoints > 0;
  // calculate total points
  var iTotalPoints = parseInt(iPointsPerArticle * (iImagePoints + iArticlePoints + Number(iPlatformPoints)));
  // round to nearest 5
  iTotalPoints = roundToNearest5(iTotalPoints);
  // check score to determine color for result page
  if(iTotalPoints <= 0) {
    // save answer for result page
    aCreator[oGameModel.iCurrentCreator].isCorrect = false;
    iTotalPoints = 0;
  } else {
    // save answer for result page
    aCreator[oGameModel.iCurrentCreator].isCorrect = true;
    aCreator[oGameModel.iCurrentCreator].iPoints = iTotalPoints;
  }
  // update points
  oPlayerModel.iPoints +=  iTotalPoints;
  // check points to see if article
  // update points on UI
  _updatePoints();
  // update feedback modal
  $("#feedback-p-points").text($.i18n("9_modalfeedback_text_points1") + iTotalPoints + $.i18n("9_modalfeedback_text_points2"));
  if(bGoodImage) $("#feedback-p-image").hide(); else $("#feedback-p-image").show();
  if(bGoodArticles) $("#feedback-p-article").hide(); else $("#feedback-p-article").show();
  if(bGoodPlatform) $("#feedback-p-platform").hide(); else $("#feedback-p-platform").show();
  if(bGoodImage && bGoodArticles && bGoodPlatform) $("#feedback-p-ok").show(); else $("#feedback-p-ok").hide();
  // show feedback modal
  $("#feedbackModal").modal("toggle");
}

/**
* Executes when the user presses on the button to learn more about a topic
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedLearnMore(oEvent) {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // increase counter for badge learner
  oGameModel.iTotalLearner += 1;
  // remove notifaction from clicked learn more button
  $(event.target).find("span").hide()
  // check if badge must be displayed
  _checkBadges(6);
}


/**
* Executes when the user presses on the button to play again
*
* @public
* @author Robert Johner <robert.johner@gmail.com>
*/
function onClickedPlayAgain() {
  // play button click sound
  _playSoundEffect(oSoundEffectModel.oButton);
  // navigate user to game mode select screen
  showScreen(3);
}


/***************************************************************************
**************************   PRIVATE METHODS  ******************************
*****************************************************************************/

/**
* Check browser and auto play the audio on init screen
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
//
function _autoplayAudio() {
  // create promise
  const promise = document.querySelector('audio').play();
  // check promise
  if (promise !== undefined) {
    promise.then(() => {
      // autoplay started, play music
      onClickedMute();
      $("#music-action").removeClass();
      $("#music-action").addClass("fa fa-volume-up red");
    }).catch(error => {
      // autoplay was prevented from browser
      // mute game styling
      $("#music-action").removeClass();
      $("#music-action").addClass("fa fa-volume-mute red");
    });
  }
}

/**
* Play background sound in loop
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _playSoundInLoop() {
  // enable loop
  oSound.loop = true;
  // load sound
  oSound.load();
  // play sound
  oSound.play();
}

/**
* Pause background sound
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _pauseSound() {
  // pause
  oSound.pause();
}

/**
* Convience method to check if game is muted
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @return boolean if game is muted
*/
function _isGameMuted() {
  // return current value from player model
  return oPlayerModel.bIsMuted;
}

/**
* Convience method to mute/unmute game
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bMute} [boolean] to mute or unmute the game
*/
function _muteGame(bMute) {
  // set boolean
  oPlayerModel.bIsMuted = bMute;
}

/**
* Plays single sound effect if user has not muted the game.
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {oEffect} [Sound] the sound effect to play
*/
function _playSoundEffect(oEffect) {
  // check if game is not muted
  if (!oPlayerModel.bIsMuted) {
    // play effect only once
    oEffect.play();
  }
}

/**
* Shakes a button by adding and removing the corresponding css classes.
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {oButton} [button] the button to shake
*/
function _shakeButton(oButton) {
  // add fancy shake effect to button
  oButton.addClass("shake");
  // remove shake effect class after animation
  oButton.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function (e) {
    oButton.delay(200).removeClass('shake');
  });
}

/**
* Shows error message with a anmimation effects. Error message
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _showErrorMessageWithAnimation(oObject) {
  // show animation
  oObject.show(1000, function () {
    // Animation complete.
  });
  // remove animation after a second
  setTimeout(function () {
    oObject.hide(1000, function () {
      // Animation complete.
    });
  }, 2500)
}

/**
* Displays the previous avatar to the user and updates the current avatar in the game model and view
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {iIndex} [Integer] the current avatar index
* @param {iMaxLength} [Integer] the number of all avatars
* @returns previous current avatar index
*/
function _prevAvatar(iIndex, iMaxLength) {
  // check if current avatar is first avatar --> set iIndex to max array value to display last avatar
  if (iIndex === 0) iIndex = iMaxLength;
  // decrement by 1
  iIndex -= 1;
  // return new index value
  return iIndex;
}

/**
* Displays the previous avatar to the user and updates the current avatar in the game model and view
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {iIndex} [Integer] the current avatar index
* @param {iMaxLength} [Integer] the number of all avatars
* @returns next current avatar index
*/
function _nextAvatar(iIndex, iMaxLength) {
  // increment by 1
  iIndex += 1;
  // return first element when last is reached
  iIndex %= iMaxLength;
  // return new index value
  return iIndex;
}

/**
* Displays the FAQ to the user
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _loadHelp() {
  // load faq array from json
  var aFaqs = oGameData.data.faqs,
  // create empty html string
  sHTML = "";
  // iterate over faq and build html
  for (var i = 0; i < aFaqs.length; i++) {
    var alternate = i % 2 === 0 ? '' : '-alternate';
    var detective = i === 1 ? "id='help-detective-img'" : "";
    sHTML += "<div class='row faq-box" + alternate + "'><div class='col-lg-2 faq-img-container'>";
    sHTML += "<img "+ detective +" src='" + aFaqs[i].image + "' class='img-fluid'></div>";
    sHTML += "<div class='col-lg-10 faq-text-container'><h5 class='faq-title" + alternate + "'>" + (oGameModel.sCurrentLang === "en" ? aFaqs[i].titleEN : aFaqs[i].titleDE) + "</h5>";
    sHTML += "<p class='faq-body" + alternate + "'>" + (oGameModel.sCurrentLang === "en" ? aFaqs[i].contentEN : aFaqs[i].contentDE) + "</p></div>";
    sHTML += "</div>";

  }
  // set faq content
  $("#faqs").html(sHTML);
}

/**
* Returns a new array with random elements
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {arr} [Array] the array
* @param {i} [Integer] the amount of elements
* @returns new random array with amount of elements
*/
function _getRandomElements(arr, i) {
  var aResult = [],
  iItems = i;
  len = arr.length;
  do {
    // create random index
    var x = Math.floor(Math.random() * len);
    // check if already added
    if (!aResult.filter(function (e) { return e.ID === arr[x].ID; }).length > 0) {
      // not already added --> add
      aResult.push(arr[x]);
      i--;
    } else {
      // already added
      i++;
    }
  } while (aResult.length !== iItems)
  return aResult;
}

/**
* Changes background image according to css class
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {sBg} [String] the class of the background
*/
function _setBackground(sBg) {
  // remove current background
  $("body").removeClass();
  // set new background
  $("body").addClass(sBg);

  // check specific UI handling for current screen
  switch (oGameModel.iCurrentScreen) {
    case 1:
    // remove active class from conainer to properly display background
    $(".game-page-container").removeClass("active");
    break;
  }
}

/**
* Shows or hides language changer
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showLanguageChanger(bShow) {
  // check if language changer should be visible
  if (bShow) {
    // show language select
    $("#choose-language").show();
  } else {
    // hide language select
    $("#choose-language").hide();
  }
}

/**
* Shows or hides close btton
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showCloseButton(bShow) {
  // check specific UI handling for current screen
  switch (oGameModel.iCurrentScreen) {
    case 2:
    case 3:
    // attach red css class to close button
    $(".close-page").addClass("red-close-icon");
    break;
    default:
    // remove red color from quit game button
    $(".close-page").removeClass("red-close-icon");
    break;
  }

  // check if button should be visible
  if (bShow) {
    // show button
    $(".close-page").show()
  } else {
    // show button
    $(".close-page").hide();
  }
}

/**
* Shows or hides avater with player name
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showCurrentUser(bShow) {
  // check if avatar should be visible
  if (bShow) {
    // show current user with avatar
    $("#current-user").show();
    $("#current-user-avatar").attr('src', oPlayerModel.sAvatarImage);
    $("#current-username").text(oPlayerModel.sPlayerName);
  } else {
    // hide avatar
    $("#current-user").hide();
  }
}

/**
* Shows or hides go back button
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showGoBackButton(bShow) {
  // check if button should be visible
  if (bShow) {
    // show button
    $(".close-page .btn-back").show();
  } else {
    // hide button
    $(".close-page .btn-back").hide();
  }
}

/**
* Shows or hides invite buton
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showInviteButton(bShow) {
  // check if button should be visible
  if (bShow) {
    // show button
    $(".invite-container").show();
  } else {
    // hide button
    $(".invite-container").hide();
  }
}

/**
* Shows or hides points label
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {bShow} [Boolean] boolean to set visibility
*/
function _showPointsLabel(bShow) {
  // set current points
  $("#current-points").text(oPlayerModel.iPoints);
  // check if label should be visible
  if (bShow) {
    // show label
    $(".detective-points-container").show();
  } else {
    // hide label
    $(".detective-points-container").hide();
  }
}

/**
* Stops timer
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _stopTimer() {
  // check if timer instance created
  if (oTimer !== undefined) {
    // clear interval
    clearInterval(oTimer);
    // reset timer
    iTimerWidth = 100;
    time = iTimePerQuestion;
    // check game mode to load correct questions
    if(oPlayerModel.sGameMode === "detective") {
      $("#progress-bar span").text(time + " " + $.i18n("4_detective_timeLeftSeconds"));
      $("#progress-bar").stop();
    } else if(oPlayerModel.sGameMode === "creator"){
      $("#progress-bar-creator span").text(time + " " + $.i18n("4_detective_timeLeftSeconds"));
      $("#progress-bar-creator").stop();
    }
  }
}

/**
* Starts the game mode detective
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _playAsDetective() {
  // load all detective questions according to language
  switch (oGameModel.sCurrentLang) {
    case "en":
    oGameQuestions = oGameData.data.questionsEN;
    break;
    case "de":
    oGameQuestions = oGameData.data.questionsDE;
    break;
  }
  // reset detective index
  oGameModel.iCurrentDetective = 0;
  // extract 5 random questions from array of all detective questions
  aDetective = _getRandomElements(oGameQuestions, iDetectivePerGame);
  // reset progress bar
  $("#questions-progress-bar").empty();
  // loop over all detective questions and populate progress bar with amount of questions
  for (var i = 0; i < aDetective.length; i++) {
    $("#questions-progress-bar").html($("#questions-progress-bar").html() + "<div class='current-question'></div>");
  }
  // show first question
  _showNextDetectiveQuestion();
}

/**
* Shows next detective question
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _showNextDetectiveQuestion() {
  // check visiblity
  if (!!$(".screen[question]:visible").length) {
    // check if last question reached
    if (aDetective.length !== 0 && oGameModel.iCurrentDetective < aDetective.length) {
      // set timer
      $("#progress-bar").stop(true, false);
      $("#progress-bar").delay(10).width('100%').delay(10).animate({ width: 0 }, iTimePerQuestion * 1000, "linear");
      // start new timer
      oTimer = setInterval(function () {
        return _timer;
      }(), 1000);
      // update progess bar on UI
      _updateProgressBarDetective();
      // update points on UI
      _updatePoints();

      // set image of question
      if (aDetective[oGameModel.iCurrentDetective].image !== undefined) {
        $("#question-img").attr('src', aDetective[oGameModel.iCurrentDetective].image);
      }
      // set title of question
      if (aDetective[oGameModel.iCurrentDetective].title !== undefined) {
        $("#question-text").text(aDetective[oGameModel.iCurrentDetective].title);
      }

      // shuffle answers to randomize correct answer position
      aDetective[oGameModel.iCurrentDetective].choices = aDetective[oGameModel.iCurrentDetective].choices
      .map((a) => ({ sort: Math.random(), value: a }))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value)

      var sChoices = "";
      // iterate over choices
      for (var i = 0; i < aDetective[oGameModel.iCurrentDetective].choices.length; i++) {
        sChoices += '<button class="btn col-5 btn-primary-red choice" ';
        sChoices += 'correct="' + aDetective[oGameModel.iCurrentDetective].answer + '"  ';
        sChoices += 'question="' + aDetective[oGameModel.iCurrentDetective].ID + '"  onclick="onClickedChoiceDetective(this)">' + aDetective[oGameModel.iCurrentDetective].choices[i] + '</button>';
      }
      // set html
      $("#choices").html(sChoices);
    } else {
      // detective mode over, reset
      oGameModel.iCurrentDetective = 0;
      // stop timer
      _stopTimer();
      // increase detective badge
      oGameModel.iTotalDetective++;
      // increase games played badge
      oGameModel.iTotalGames++;
      // check if player obtained badges and navigate to result page
      _checkBadges(6);
    }
  }
}

/**
* Starts the game mode detective
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _playAsCreator() {
  // load all creator questions according to language
  switch (oGameModel.sCurrentLang) {
    case "en":
    oGameQuestions = oGameData.data.creatorQuestionsEN;
    break;
    case "de":
    oGameQuestions = oGameData.data.creatorQuestionsDE;
    break;
  }
  // reset creator index
  oGameModel.iCurrentCreator = 0;
  // extract 5 random questions from array of all creator questions
  aCreator = _getRandomElements(oGameQuestions, iCreatorPerGame);
  // reset progress bar
  $("#creator-questions-progress-bar").empty();
  // loop over all detective questions and populate progress bar with amount of questions
  for (var i = 0; i < aCreator.length; i++) {
    $("#creator-questions-progress-bar").html($("#creator-questions-progress-bar").html() + "<div class='current-creator'></div>");
  }
  // show first question
  _showNextCreatorQuestion();
}


/**
* Shows next creator question
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _showNextCreatorQuestion() {
  // check visiblity
  if (!!$(".screen[question]:visible").length) {
    // check if last question reached
    if (aCreator.length !== 0 && oGameModel.iCurrentCreator < aCreator.length) {
      // reset view
      _resetCreatorView();
      // set timer
      $("#progress-bar-creator").stop(true, false);
      $("#progress-bar-creator").delay(10).width('100%').delay(10).animate({ width: 0 }, iTimePerQuestion * 1000, "linear");
      // start new timer
      oTimer = setInterval(function () {
        return _timer;
      }(), 1000);
      // update progess bar on UI
      _updateProgressBarCreator();
      // update points on UI
      _updatePoints();
      // set title
      $('#article-title').css("top", "200px");
      // get current creator
      var oCurrentCreator = aCreator[oGameModel.iCurrentCreator];
      // set title
      $("#article-title").html(oCurrentCreator.title);
      // animate title and image box
      setTimeout(function () {
        $('#article-title').animate({ top: 0 }, 1000, function () {
          //animate
        });
        setTimeout(function () {
          $('#img-section').css("visibility", "visible");
        }, 1000);
      }, 500);

      // insert images from current creator
      $("#images").html("<div class='img-section-img' onclick='onClickedCreatorImg(this)'><img src="+oCurrentCreator.image1.src+"></div>");
      $("#images").html($("#images").html() + "<div class='img-section-img' onclick='onClickedCreatorImg(this)'><img src="+oCurrentCreator.image2.src+"></div>");

      // insert author and date text in article
      $("#article").html("<p><span style='font-weight:bold;'>"+ $.i18n("5_creator_article_author") + " " + oPlayerModel.sPlayerName + "</span></p> <p style='margin-top:-15px;'><span style='font-weight:bold;'>" + $.i18n("5_creator_article_date") + " " + _getDate() +"</span></p>");

      // loop over text options
      for (let i = 0; i < oCurrentCreator.articleText.length; i++) {
        // get text
        if (oCurrentCreator.articleText[i].selectText) {
          // check if combobox need to be rendered
          switch (Object.keys(oCurrentCreator.articleText[i].text).length) {
            case 1:
            $("#article").html($("#article").html() + `<div class="select"><select onchange="onChangedArticleText()" name="slct" id="slct" class="slct"><option selected disabled value>`+ $.i18n("5_creator_article_choose") +`</option><option value=${oCurrentCreator.articleText[i].text.option1.points}>${oCurrentCreator.articleText[i].text.option1.text}</option></select></div>`);
            break
            case 2:
            $("#article").html($("#article").html() + `<div class="select"><select onchange="onChangedArticleText()" name="slct" id="slct" class="slct"><option selected disabled value>`+ $.i18n("5_creator_article_choose") +`</option><option value=${oCurrentCreator.articleText[i].text.option1.points}>${oCurrentCreator.articleText[i].text.option1.text}</option><option value=${oCurrentCreator.articleText[i].text.option2.points}>${oCurrentCreator.articleText[i].text.option2.text}</option></select></div>`);
            break
            case 3:
            $("#article").html($("#article").html() + `<div class="select"><select onchange="onChangedArticleText()" name="slct" id="slct" class="slct"><option selected disabled value>`+ $.i18n("5_creator_article_choose") +`</option><option value=${oCurrentCreator.articleText[i].text.option1.points}>${oCurrentCreator.articleText[i].text.option1.text}</option><option value=${oCurrentCreator.articleText[i].text.option2.points}>${oCurrentCreator.articleText[i].text.option2.text}</option><option value=${oCurrentCreator.articleText[i].text.option3.points}>${oCurrentCreator.articleText[i].text.option3.text}</option></select></div>`);
            break;
            case 4:
            $("#article").html($("#article").html() + `<div class="select"><select onchange="onChangedArticleText()" name="slct" id="slct" class="slct"><option selected disabled value>`+ $.i18n("5_creator_article_choose") +`</option><option value=${oCurrentCreator.articleText[i].text.option1.points}>${oCurrentCreator.articleText[i].text.option1.text}</option><option value=${oCurrentCreator.articleText[i].text.option2.points}>${oCurrentCreator.articleText[i].text.option2.text}</option><option value=${oCurrentCreator.articleText[i].text.option3.points}>${oCurrentCreator.articleText[i].text.option3.text}</option><option value=${oCurrentCreator.articleText[i].text.option4.points}>${oCurrentCreator.articleText[i].text.option4.text}</option></select></div>`);
          }
        } else {
          // create span element with simple text
          $("#article").html($("#article").html() + `<span>${oCurrentCreator.articleText[i].text}</span>`);
        }
      }
    } else {
      // creator mode over, reset
      oGameModel.iCurrentCreator = 0;
      // stop timer
      _stopTimer();
      // increase creator badge
      oGameModel.iTotalCreator++;
      // increase games played badge
      oGameModel.iTotalGames++;
      // check if player obtained badges and navigate to result page
      _checkBadges(6);
    }
  }
}
/**
* Resets creator view on UI
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _resetCreatorView(){
  // reset images
  $("#images").html("");
  // hide section
  $("#img-section").removeAttr('style').css("visiblity", "hidden");
  // clear article section
  $("#article").html("");
  // hide section
  $("#article-section").removeAttr('style').css("visiblity", "hidden");
  // reset platform
  $(".platform-icon").removeClass("selected");
  // hide section
  $("#platform-section").removeAttr('style').css("visiblity", "hidden");
}

/**
* Gets todays date and returns it
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @return returns todays date
*/
function _getDate() {
  // get today
  var oDate = new Date(),
  iMonth = oDate.getMonth()+1,
  iDay = oDate.getDate();
  // format date and return
  return ((''+iDay).length<2 ? '0' : '') + iDay + '.' + ((''+iMonth).length<2 ? '0' : '') + iMonth + '.'  + oDate.getFullYear();
}

/**
* Adds badge to array if not already collected
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _addBadge(sBadge) {
  // check if badge is not yet collected
  if(!sBadge.bCollected) {
    // push badge to array
    aBadges.push(sBadge);
  }
}

/**
* Checks if badge modal must be displayed with obtained badge.
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
* @param {iScreen} [Integer] screen to show
*/
function _checkBadges(iScreen) {
  // badge to be displayed
  var sBadge = "";
  // check all badge progress
  // check games played badges
  switch (oGameModel.iTotalGames) {
    // award player after first game
    case 1:
    sBadge = oGameData.data.badges.games1;
    _addBadge(sBadge);
    break;
    // award player after 10 games with advanced badge
    case 10:
    sBadge = oGameData.data.badges.games2;
    _addBadge(sBadge);
    break;
    // award player after 50 games with expert badge
    case 50:
    sBadge = oGameData.data.badges.games3;
    _addBadge(sBadge);
    break;
  }

  // check points badge
  switch (true) {
    // award player for 500 points
    case (oGameModel.iTotalPoints > 499 && oGameModel.iTotalPoints < 99999):
    sBadge = oGameData.data.badges.points1;
    _addBadge(sBadge);
    break;
    // award player for 10000 points
    case (oGameModel.iTotalPoints > 99999 && oGameModel.iTotalPoints < 49999):
    sBadge = oGameData.data.badges.points2;
    _addBadge(sBadge);
    break;
    // award player for 50000 points
    case (oGameModel.iTotalPoints > 49999):
    sBadge = oGameData.data.badges.points3;
    _addBadge(sBadge);
    break;
  }

  // check learner badge
  switch (oGameModel.iTotalLearner) {
    // award player after first learning
    case 1:
    sBadge = oGameData.data.badges.learner1;
    _addBadge(sBadge);
    break;
    // award player for 30 learning
    case 30:
    sBadge = oGameData.data.badges.learner2;
    _addBadge(sBadge);
    break;
    // award player for 100 learning
    case 100:
    sBadge = oGameData.data.badges.learner3;
    _addBadge(sBadge);
    break;
  }

  // check detective badge
  switch (oGameModel.iTotalDetective) {
    // award player 3 detective
    case 3:
    sBadge = oGameData.data.badges.detective1;
    _addBadge(sBadge);
    break;
    // award player 50 detective
    case 50:
    sBadge = oGameData.data.badges.detective2;
    _addBadge(sBadge);
    break;
    // award player 100 detective
    case 100:
    sBadge = oGameData.data.badges.detective3;
    _addBadge(sBadge);
    break;
  }

  // check creator badge
  switch (oGameModel.iTotalCreator) {
    // award player 3 creator
    case 3:
    sBadge = oGameData.data.badges.creator1;
    _addBadge(sBadge);
    break;
    // award player for 50 creator
    case 50:
    sBadge = oGameData.data.badges.creator2;
    _addBadge(sBadge);
    break;
    // award player for 100 creator
    case 100:
    sBadge = oGameData.data.badges.creator3;
    _addBadge(sBadge);
    break;
  }

  // check if must be collected
  if (aBadges.length > 0) {
    // get last badge
    sBadge = aBadges.pop();
    // set collected to true
    sBadge.bCollected = true;
    // toggle modal
    $("#earnedBadgeModal").modal("toggle");
    // set value of button to navigate to correct screen
    $("#close-badge").val(iScreen);

    // check language
    switch (oGameModel.sCurrentLang) {
      case "en":
      $("#earned-badge-title").text(sBadge.titleEN);
      $("#earned-badge-text").text(sBadge.textEN);
      break;
      case "de":
      $("#earned-badge-title").text(sBadge.titleDE);
      $("#earned-badge-text").text(sBadge.textDE);
      break;
    }

    // dynamically set src of the badge
    $("#earned-badge").attr("src", sBadge.image);

    // remove badge from array
  } else {
    // check if screen must be changed
    if (iScreen !== oGameModel.iCurrentScreen) {
      // go directly to screen withouth showing a badge
      showScreen(iScreen);
    }
  }
}

/**
* Updates progess bar for the game mode detective
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _updateProgressBarDetective() {
  // remove old current questions
  if ($(".current-question")[oGameModel.iCurrentDetective - 1]) {
    $(".current-question")[oGameModel.iCurrentDetective - 1].classList.remove("active");
  }
  // set current question in progress bar
  $(".current-question")[oGameModel.iCurrentDetective].classList.add("active");
  // generate html
  var sText = $.i18n("4_detective_progess") + " " + (oGameModel.iCurrentDetective + 1) + "/" + aDetective.length;
  // set progress text
  $("#question-number").text(sText);
}

/**
* Updates progess bar for the game mode creator
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _updateProgressBarCreator() {
  // remove old current questions
  if ($(".current-creator")[oGameModel.iCurrentCreator - 1]) {
    $(".current-creator")[oGameModel.iCurrentCreator - 1].classList.remove("active");
  }
  // set current question in progress bar
  $(".current-creator")[oGameModel.iCurrentCreator].classList.add("active");
  // generate html
  var sText = $.i18n("5_creator_progess") + " " + (oGameModel.iCurrentCreator + 1) + "/" + aCreator.length;
  $("#creator-question-number").text(sText);
}

/**
* Updates points label
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _updatePoints() {
  // update points on UI
  $(".points").text(oPlayerModel.iPoints);
  // update points on model
  oGameModel.iTotalPoints = oPlayerModel.iPoints;
}

/**
* Generates and the result page for the game mode detective with an overview of the perfomance and the learn more links.
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _previewResults() {
  var sHtml = "",
  sLabel = "",
  sResultTitle = "",
  aResult = [];
  // update points
  _updatePoints();
  // check game mode to load correct questions
  if(oPlayerModel.sGameMode === "detective") {
    aResult = aDetective;
  } else if(oPlayerModel.sGameMode === "creator"){
    aResult = aCreator;
  }

  // iterate over all questions
  for (var i = 0; i < aResult.length; i++) {
    // get result title for each question to display on page
    if (aResult[i].resultTitle !== undefined) {
      sResultTitle = aResult[i].resultTitle;
    } else {
      sResultTitle = '';
    }
    sResultTitle += ", " + $.i18n("6_result_points");
    if (aResult[i].isCorrect === true) {
      // set correct css class
      sLabel = 'success';
      if(oPlayerModel.sGameMode === "detective") {
        sResultTitle += " +" + iPointsPerQuestion;
      } else if(oPlayerModel.sGameMode === "creator"){
        sResultTitle += " +" + aResult[i].iPoints;
      }
    } else if (aResult[i].isCorrect === false) {
      // set error css class
      sLabel = 'error';
      sResultTitle += " 00";
    } else {
      // set neutral css class
      sLabel = 'warning';
      sResultTitle += $.i18n("6_result_timeRanOut");
    }
    var n = i + 1;
    // generate html content
    sHtml += '<div class="row result ' + sLabel + ' bs">';
    sHtml += '<div class="col-md-6"><div class="result-question">';
    sHtml += '<p>' + $.i18n("question") + ' #' + n + ': ' + sResultTitle + ' </p></div></div>';

    sHtml += '<div class="col-md-6"><div class="result-link">';
    sHtml += '<a href="' + aResult[i].learn + '" target="_blank"><button onclick="onClickedLearnMore(this)" class="btn btn-blue' + (aResult[i].isCorrect === false ? ' waggle">' : '">');
    sHtml += '<i class="fas fa-external-link-alt"></i> ' + $.i18n("6_result_learnMore") + (aResult[i].isCorrect === false ? '<span class="notification">1</span>' : '');
    sHtml += '</button></a></div></div></div>';
  }
  // set result content
  $("#preview-results").html(sHtml);
}


var time = iTimePerQuestion;

/**
* Generates and manages timer for detective and creator mode
*
* @private
* @author Robert Johner <robert.johner@gmail.com>
*/
function _timer() {
  // update timers
  $("#progress-bar span").text(time + " " + $.i18n("4_detective_timeLeftSeconds"));
  $("#progress-bar-creator span").text(time + " " + $.i18n("5_creator_timeLeft"));

  // check time to change colors
  if (time > 30) {
    $(".progress-container").addClass("green");
    $(".progress-container").removeClass("yellow");
    $(".progress-container").removeClass("red");
  } else if (time <= 30 && time > 10) {
    $(".progress-container").addClass("yellow");
    $(".progress-container").removeClass("green");
    $(".progress-container").removeClass("red");
  } else if (time <= 10) {
    $(".progress-container").addClass("red");
    $(".progress-container").removeClass("green");
    $(".progress-container").removeClass("yellow");
    // check if countdown is not already playing
    if (!bCountDownPlaying) {
      // play countdown sound
      _playSoundEffect(oSoundEffectModel.oCountDown);
      // set countdown playing to true
      bCountDownPlaying = true;
    }
  }
  // calculate width
  iTimerWidth = iTimerWidth - 1.6;
  // reduce time
  time--;
  // check if time ran out
  if (time === -1) {
    // set width to 0
    $("#progress-bar").width(0);
    // play neutral sound effect
    _playSoundEffect(oSoundEffectModel.oNeutral);
    // clear previous progress bar
    if (oTimer !== undefined) {
      clearInterval(oTimer);
      iTimerWidth = 100;
      time = iTimePerQuestion;
      // reset timer classes
      $(".progress-container").removeClass("green");
      $(".progress-container").removeClass("yellow");
      $(".progress-container").removeClass("red");
    }
    // reset countdown trigger
    bCountDownPlaying = false;
    // go to next question
    setTimeout(function () {
      // play swipe sound
      _playSoundEffect(oSoundEffectModel.oSwipe);
      // check game mode to load correct questions
      if(oPlayerModel.sGameMode === "detective") {
        // increase current detective index
        oGameModel.iCurrentDetective++;
        // set new time
        $("#progress-bar span").text(time + " " + $.i18n("4_detective_timeLeftSeconds"));
        // show next detective
        _showNextDetectiveQuestion();
      } else if(oPlayerModel.sGameMode === "creator"){
        // reset creator view
        _resetCreatorView();
        // increase current detective index
        oGameModel.iCurrentCreator++;
        // set new time
        $("#progress-bar-creator span").text(time + " " + $.i18n("5_creator_timeLeft"));
        // show next creator
        _showNextCreatorQuestion();
      }
    }, 500)
  }
}
