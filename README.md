<img src="img/logo.png" align="right" style="width: 15%;border-radius: 5px;" />

# FightFakeNews
This readme covers following topics:

  - **Introduction.** *A brief introduction to FightFakeNews.*
  - **Programming Guide.** *A brief description of the project.*
  - **User Guide.** *A brief description on how to launch or extend the project.*
  - **Deployment Guide.** *A brief description on how to deploy the project.*
  - **History.** *Check what has changed.*

# Introduction!
FightFakeNews is a **digital gamified learning** approach to improve digital media literacy in players. The goal of FightFakeNews is to **reslience players against Fake News** in a holistic approach of dealing with fake news by providing the players with two different game modes "**Detective**" and "**Creator**".

As **detective**, players are confronted with news that they have to judge by their credibility or answer questions about fake news characteristics to learn about the fundamental basics about Fake News.
As **creator** on the other hand, the player intentionally creates and spreads fake news in order to reach a certain goal.

In FightFakeNews you can choose if you want to be a Fake News Detective or a Fake News Creator. As a detective, you like to solve puzzles and find Fake News because if theres is one thing that you hate then its Fake News. On the contrary if you want to be an evil genious that spreads Fake News, you can also do that as a creator of Fake News. But remember, to become a complete mastermind of Fake News, you need the best skills of both worlds to survive in the cruel world of misinformation and score as many points as possible! What are you waiting for?

<p align="center">
 <img src="img/00_init.png?raw=true" alt="FightFakeNews"/>
</p>

<p align="center">
 <img src="img/01_select.png?raw=true" alt="Game Mode select"/>
</p>


<p align="center">
 <img src="img/02_detective.png?raw=true" alt="Detective"/>
</p>


<p align="center">
 <img src="img/03_creator.png?raw=true" alt="Creator"/>
</p>


# Programming Guide!
### Programming environment
The project is realized in native HTML, JS, CSS and JSON. The frameworks Bootstrap and jQuery were mainly used to create FightFakeNews. Different plugins such as a preloader and jQuery.i18n are included to simplify the development process. The game can be configured in a JSON file to add, update or remove content from each game mode.

### Project structure
The project is structured as follow:

    .
    ├── assets               # contains all assets
      ├── css                # contains css files
      ├── data               # contains data files
      ├── i18n               # contains translations
      ├── img                # contains images
      ├── js                 # contains js files
      ├── sound              # contains sound files
    ├── img                  # contains images for the git repo
    ├── min                  # contains the whole project in a minified version for deployment
    ├── index.html           # static html file
    ├── .gitignore
    └── README.md

# User Guide!
### Prereqisites
To launch the project you need at least following requirements:
  - A local webserver
  - An IDE


### Setting up
To launch the project:
1. Clone this git repo
    ```
    git clone https://gitlab.com/jor93/fightfakenews.git
    ```
2. Fire up your favorite IDE and import the project
3. Copy the project to a webserver of your choice

# Deployment Guide!
To deploy the project, simple copy all the content of the "min" folder into the root folder of your public webserver. When building a new version of the project, do not forget to minimize all required files and replace them in the "min" folder to enhance the loading speed of the game. 

# History!
- 06.12.2020 added deployment guide
- 26.11.2020 fixed bugs reported by testers
- 25.11.2020 initial commit

*made with ♥ by Robert Johner*
